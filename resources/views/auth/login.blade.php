@extends('auth.index')
@section('content')
<header class="d-flex justify-content-between align-items-center">
</header>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-5 pa-0">
            <div class="owl-carousel dots-on-item owl-theme" id="owl_demo_1">
                <div class="fadeOut item auth-cover-img overlay-wrap" style="background-image:url(dist/img/bg2.jpg);">
                    <div class="auth-cover-info py-xl-0 pt-100 pb-50">
                        <div class="auth-cover-content text-center w-xxl-75 w-sm-90 w-xs-100">
                            <h1 class="display-3 text-white mb-20">
                                Understand and look deep into nature.
                            </h1>
                            <p class="text-white">
                                The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout. Again during the 90s as desktop publishers bundled the text with their software.
                            </p>
                        </div>
                    </div>
                    <div class="bg-overlay bg-trans-dark-50">
                    </div>
                </div>
                <div class="fadeOut item auth-cover-img overlay-wrap" style="background-image:url(dist/img/bg1.jpg);">
                    <div class="auth-cover-info py-xl-0 pt-100 pb-50">
                        <div class="auth-cover-content text-center w-xxl-75 w-sm-90 w-xs-100">
                            <h1 class="display-3 text-white mb-20">
                                Experience matters for good applications.
                            </h1>
                            <p class="text-white">
                                The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software.
                            </p>
                        </div>
                    </div>
                    <div class="bg-overlay bg-trans-dark-50">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-7 pa-0">
            <div class="auth-form-wrap py-xl-0 py-50">
                <div class="auth-form w-xxl-55 w-xl-75 w-sm-90 w-xs-100">
                    @include('_partial.notification')
                    {!! Form::open(['route' => 'login.store', 'method' => 'post']) !!}
                        {{ csrf_field() }}
                        <h1 class="display-4 mb-10">
                            Welcome Back :)
                        </h1>
                        <p class="mb-30">
                            Sign in to your account and enjoy unlimited perks.
                        </p>
                        <div class="form-group">
                            <input class="form-control" placeholder="NIK" name="nik" required autofocus>
                            </input>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control" placeholder="Password" type="password" name="password">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <span class="feather-icon">
                                                <i data-feather="eye-off">
                                                </i>
                                            </span>
                                        </span>
                                    </div>
                                </input>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">
                            Login
                        </button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
