<!DOCTYPE html>
<!-- 
Template Name: Pangong - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: https://hencework.ticksy.com/
License: You must have a valid license purchased only from themeforest to legally use the template for your project.
-->
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
        <title>
            Pangong I Login
        </title>
        <meta content="A responsive bootstrap 4 admin dashboard template by hencework" name="description"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon -->
        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <!-- Custom CSS -->
        <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!-- Preloader -->
        <div class="preloader-it">
            <div class="loader-pendulums">
            </div>
        </div>
        <!-- /Preloader -->
        <!-- HK Wrapper -->
        <div class="hk-wrapper">
            <!-- Main Content -->
            <div class="hk-pg-wrapper hk-auth-wrapper">
                @yield('content')
            </div>
            <!-- /Main Content -->
        </div>
        <!-- /HK Wrapper -->
           <!-- jQuery -->
        <script src="dist/vendors/jquery/dist/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="dist/vendors/popper.js/dist/umd/popper.min.js"></script>
        <script src="dist/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Slimscroll JavaScript -->
        <script src="dist/js/jquery.slimscroll.js"></script>

        <!-- Fancy Dropdown JS -->
        <script src="dist/js/dropdown-bootstrap-extended.js"></script>

        <!-- Owl JavaScript -->
        <script src="dist/vendors/owl.carousel/dist/owl.carousel.min.js"></script>

        <!-- FeatherIcons JavaScript -->
        <script src="dist/js/feather.min.js"></script>

        <!-- Init JavaScript -->
        <script src="dist/js/init.js"></script>
        <script src="dist/js/login-data.js"></script>
    </body>
</html>