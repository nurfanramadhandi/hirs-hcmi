@if ($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
                <li> <span class="text-danger">{{ $error }}</span></li>
            @endforeach
        </ul>
@endif

@if (Session::get('danger'))
	<div class="alert alert-danger" role="alert">
	   	{{ Session::get('danger') }}
	</div>
@endif

@if (Session::get('success'))
	<div class="alert alert-success" role="alert">
	   	{{ Session::get('success') }}
	</div>
@endif
