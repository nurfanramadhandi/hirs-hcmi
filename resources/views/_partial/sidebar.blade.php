<!-- Vertical Nav -->
<nav class="hk-nav hk-nav-dark">
    <a class="hk-nav-close" href="javascript:void(0);" id="hk_nav_close">
        <span class="feather-icon">
            <i data-feather="x">
            </i>
        </span>
    </a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">
                <li class="nav-item active">
                    <a class="nav-link" data-target="#dash_drp" data-toggle="collapse" href="javascript:void(0);">
                        <span class="feather-icon">
                            <i data-feather="activity">
                            </i>
                        </span>
                        <span class="nav-link-text">
                            Dashboard
                        </span>
                    </a>
                    <ul class="nav flex-column collapse collapse-level-1" id="dash_drp">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item active">
                                    <a class="nav-link" href="dashboard1.html">
                                        CRM
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="dashboard2.html">
                                        Project
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="dashboard3.html">
                                        Statistics
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link link-with-badge" data-target="#app_drp" data-toggle="collapse" href="javascript:void(0);">
                        <span class="feather-icon">
                            <i data-feather="package">
                            </i>
                        </span>
                        <span class="nav-link-text">
                            Application
                        </span>
                        <span class="badge badge-primary badge-pill">
                            4
                        </span>
                    </a>
                    <ul class="nav flex-column collapse collapse-level-1" id="app_drp">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="chats.html">
                                        Chat
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="calendar.html">
                                        Calendar
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="email.html">
                                        Email
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="file-manager.html">
                                        File Manager
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-target="#auth_drp" data-toggle="collapse" href="javascript:void(0);">
                        <span class="feather-icon">
                            <i data-feather="zap">
                            </i>
                        </span>
                        <span class="nav-link-text">
                            Authentication
                        </span>
                    </a>
                    <ul class="nav flex-column collapse collapse-level-1" id="auth_drp">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" data-target="#signup_drp" data-toggle="collapse" href="javascript:void(0);">
                                        Sign Up
                                    </a>
                                    <ul class="nav flex-column collapse collapse-level-2" id="signup_drp">
                                        <li class="nav-item">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="signup.html">
                                                        Cover
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="signup-simple.html">
                                                        Simple
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-target="#signin_drp" data-toggle="collapse" href="javascript:void(0);">
                                        Login
                                    </a>
                                    <ul class="nav flex-column collapse collapse-level-2" id="signin_drp">
                                        <li class="nav-item">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="login.html">
                                                        Cover
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="login-simple.html">
                                                        Simple
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-target="#recover_drp" data-toggle="collapse" href="javascript:void(0);">
                                        Recover Password
                                    </a>
                                    <ul class="nav flex-column collapse collapse-level-2" id="recover_drp">
                                        <li class="nav-item">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="forgot-password.html">
                                                        Forgot Password
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="reset-password.html">
                                                        Reset Password
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="lock-screen.html">
                                        Lock Screen
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="404.html">
                                        Error 404
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="maintenance.html">
                                        Maintenance
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-target="#pages_drp" data-toggle="collapse" href="javascript:void(0);">
                        <span class="feather-icon">
                            <i data-feather="file-text">
                            </i>
                        </span>
                        <span class="nav-link-text">
                            Pages
                        </span>
                    </a>
                    <ul class="nav flex-column collapse collapse-level-1" id="pages_drp">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="profile.html">
                                        Profile
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="invoice.html">
                                        Invoice
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="gallery.html">
                                        Gallery
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="activity.html">
                                        Activity
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="faq.html">
                                        Faq
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="hk-nav-backdrop" id="hk_nav_backdrop">
</div>
<!-- /Vertical Nav -->
