<!-- Setting Panel -->
<div class="hk-settings-panel">
    <div class="nicescroll-bar position-relative">
        <div class="settings-panel-wrap">
            <div class="settings-panel-head">
                <img alt="brand" class="brand-img d-inline-block align-top" src="dist/img/logo-light.png"/>
                <a class="settings-panel-close" href="javascript:void(0);" id="settings_panel_close">
                    <span class="feather-icon">
                        <i data-feather="x">
                        </i>
                    </span>
                </a>
            </div>
            <hr>
                <h6 class="mb-5">
                    Layout
                </h6>
                <p class="font-14">
                    Choose your preferred layout
                </p>
                <div class="layout-img-wrap">
                    <div class="row">
                        <a class="col-6 mb-30 active" href="javascript:void(0);">
                            <img alt="layout" class="rounded opacity-70" src="dist/img/layout1.png">
                                <i class="zmdi zmdi-check">
                                </i>
                            </img>
                        </a>
                        <a class="col-6 mb-30" href="dashboard2.html">
                            <img alt="layout" class="rounded opacity-70" src="dist/img/layout2.png">
                                <i class="zmdi zmdi-check">
                                </i>
                            </img>
                        </a>
                        <a class="col-6" href="dashboard3.html">
                            <img alt="layout" class="rounded opacity-70" src="dist/img/layout3.png">
                                <i class="zmdi zmdi-check">
                                </i>
                            </img>
                        </a>
                    </div>
                </div>
                <hr>
                    <h6 class="mb-5">
                        Navigation
                    </h6>
                    <p class="font-14">
                        Menu comes in two modes: dark & light
                    </p>
                    <div class="button-list hk-nav-select mb-10">
                        <button class="btn btn-outline-light btn-sm btn-wth-icon icon-wthot-bg" id="nav_light_select" type="button">
                            <span class="icon-label">
                                <i class="fa fa-sun-o">
                                </i>
                            </span>
                            <span class="btn-text">
                                Light Mode
                            </span>
                        </button>
                        <button class="btn btn-outline-primary btn-sm btn-wth-icon icon-wthot-bg" id="nav_dark_select" type="button">
                            <span class="icon-label">
                                <i class="fa fa-moon-o">
                                </i>
                            </span>
                            <span class="btn-text">
                                Dark Mode
                            </span>
                        </button>
                    </div>
                    <hr>
                        <h6 class="mb-5">
                            Top Nav
                        </h6>
                        <p class="font-14">
                            Choose your liked color mode
                        </p>
                        <div class="button-list hk-navbar-select mb-10">
                            <button class="btn btn-outline-primary btn-sm btn-wth-icon icon-wthot-bg" id="navtop_light_select" type="button">
                                <span class="icon-label">
                                    <i class="fa fa-sun-o">
                                    </i>
                                </span>
                                <span class="btn-text">
                                    Light Mode
                                </span>
                            </button>
                            <button class="btn btn-outline-light btn-sm btn-wth-icon icon-wthot-bg" id="navtop_dark_select" type="button">
                                <span class="icon-label">
                                    <i class="fa fa-moon-o">
                                    </i>
                                </span>
                                <span class="btn-text">
                                    Dark Mode
                                </span>
                            </button>
                        </div>
                        <hr>
                            <div class="d-flex justify-content-between align-items-center">
                                <h6>
                                    Scrollable Header
                                </h6>
                                <div class="toggle toggle-sm toggle-simple toggle-light toggle-bg-primary scroll-nav-switch">
                                </div>
                            </div>
                            <button class="btn btn-primary btn-block btn-reset mt-30" id="reset_settings">
                                Reset
                            </button>
                        </hr>
                    </hr>
                </hr>
            </hr>
        </div>
    </div>
    <img alt="brand" class="d-none" src="dist/img/logo-light.png"/>
    <img alt="brand" class="d-none" src="dist/img/logo-dark.png"/>
</div>
<!-- /Setting Panel -->
