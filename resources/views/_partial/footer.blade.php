<!-- Footer -->
<div class="hk-footer-wrap container">
    <footer class="footer">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <p>
                    Pampered by
                    <a class="text-dark" href="https://hencework.com/" target="_blank">
                        Hencework
                    </a>
                    © 2019
                </p>
            </div>
            <div class="col-md-6 col-sm-12">
                <p class="d-inline-block">
                    Follow us
                </p>
                <a class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4" href="#">
                    <span class="btn-icon-wrap">
                        <i class="fa fa-facebook">
                        </i>
                    </span>
                </a>
                <a class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4" href="#">
                    <span class="btn-icon-wrap">
                        <i class="fa fa-twitter">
                        </i>
                    </span>
                </a>
                <a class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4" href="#">
                    <span class="btn-icon-wrap">
                        <i class="fa fa-google-plus">
                        </i>
                    </span>
                </a>
            </div>
        </div>
    </footer>
</div>
<!-- /Footer -->
