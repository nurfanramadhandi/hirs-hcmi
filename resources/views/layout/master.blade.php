<!DOCTYPE html>
<!-- 
Template Name: Pangong - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: https://hencework.ticksy.com/
License: You must have a valid license purchased only from themeforest to legally use the template for your project.
-->
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Pangong I CRM Dashboard</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ URL::to('') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
   {{--  <link rel="stylesheet" href="{{ url('css/app.css') }}">
    <link rel="stylesheet" href="{{ url('css/vendor.css') }}"> --}}
    <!-- vector map CSS -->
    <link href="dist/vendors/vectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" type="text/css" />

    <!-- Toggles CSS -->
    <link href="dist/vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
    <link href="dist/vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">
    
    <!-- Toastr CSS -->
    <link href="dist/vendors/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    @yield('styles')
</head>

<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->
    
    <!-- HK Wrapper -->
    <div class="hk-wrapper hk-vertical-nav">

        <!-- Top Navbar -->
        @include('_partial.topbar')
        <!-- /Top Navbar -->

        <!-- Vertical Nav -->
        @include('_partial.sidebar')
        <!-- /Vertical Nav -->

        <!-- Setting Panel -->
        @include('_partial.setting')
        <!-- /Setting Panel -->

        <!-- Main Content -->
        <div class="hk-pg-wrapper">
            <!-- Container -->
            <div class="container mt-xl-50 mt-sm-30 mt-15">
                <!-- Title -->
                <div class="hk-pg-header align-items-top">
                    <div>
                        <h2 class="hk-pg-title font-weight-600 mb-10">Customer Management</h2>
                        <p>Questions about onboarding lead data? <a href="#">Learn more.</a></p>
                    </div>
                    <div class="d-flex w-500p">
                        <select class="form-control custom-select custom-select-sm mr-15">
                            <option selected="">Latest Products</option>
                            <option value="1">CRM</option>
                            <option value="2">Projects</option>
                            <option value="3">Statistics</option>
                        </select>
                        <select class="form-control custom-select custom-select-sm mr-15">
                            <option selected="">USA</option>
                            <option value="1">USA</option>
                            <option value="2">India</option>
                            <option value="3">Australia</option>
                        </select>
                        <select class="form-control custom-select custom-select-sm">
                            <option selected="">December</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="1">April</option>
                            <option value="2">May</option>
                            <option value="3">June</option>
                            <option value="1">July</option>
                            <option value="2">August</option>
                            <option value="3">September</option>
                            <option value="1">October</option>
                            <option value="2">November</option>
                            <option value="3">December</option>
                        </select>
                    </div>
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12">
                        @yield('content')
                    </div>
                </div>
                <!-- /Row -->
            </div>
            <!-- /Container -->

            <!-- Footer -->
                @include('_partial.footer')
            <!-- /Footer -->
        </div>
        <!-- /Main Content -->

    </div>
    @yield('head-scripts')
    <!-- /HK Wrapper -->
   {{--  <script src="{{ url('js/vendor.js')}}"></script>
    <script src="{{ url('js/app.js')}}"></script> --}}
     <!-- jQuery -->
    <script src="dist/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="dist/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="dist/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="dist/js/jquery.slimscroll.js"></script>

    <!-- Fancy Dropdown JS -->
    <script src="dist/js/dropdown-bootstrap-extended.js"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="dist/js/feather.min.js"></script>

    <!-- Toggles JavaScript -->
    <script src="dist/vendors/jquery-toggles/toggles.min.js"></script>
    <script src="dist/js/toggle-data.js"></script>
    
    <!-- Counter Animation JavaScript -->
    <script src="dist/vendors/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="dist/vendors/jquery.counterup/jquery.counterup.min.js"></script>
    
    <!-- EChartJS JavaScript -->
    <script src="dist/vendors/echarts/dist/echarts-en.min.js"></script>
    
    <!-- Sparkline JavaScript -->
    <script src="dist/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
    
    <!-- Vector Maps JavaScript -->
    <script src="dist/vendors/vectormap/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="dist/vendors/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="dist/js/vectormap-data.js"></script>

    <!-- Owl JavaScript -->
    <script src="dist/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    
    <!-- Toastr JS -->
    <script src="dist/vendors/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    
    <!-- Init JavaScript -->
    <script src="dist/js/init.js"></script>
    <script src="dist/js/dashboard-data.js"></script>
    @yield('scripts')
</body>

</html>